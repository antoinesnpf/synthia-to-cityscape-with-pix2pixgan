import torch 




class Label:

    COLOR_DICT = { 
        0 : (0, 0, 0),
        1 : (185, 122, 87),
        2 : (255, 127, 39),
        3 : (127, 127, 127), 
        4 : (195, 195, 195),
        5 : (255, 242, 0),
        6 : (63, 72, 204),
        7 : (43, 177, 76),
        8 : (181, 230, 29),
        9 : (0, 162, 232),
        10 : (255, 0, 0),
        11 : (200, 191, 231),
        12 : (163, 73, 164),
        13 : (255, 174, 201)
    }

    def __init__(self, name, row_id, id) : 
        self.name = name 
        self.row_id = row_id
        self.id = id
        self.color = self.COLOR_DICT[id]
        

def shallow_to_deep(t, n_class, hash_map):
    'convert a shallow label [1, h, w] to a one-hot-encoded one [n_class, h, w]'
    
    t = t.repeat(n_class, 1, 1)
    for cls in range(n_class):
        for i, row_cls in enumerate(hash_map[cls]) : 
            if i == 0 : 
                exec_code  = f"mask = (t[cls] == {row_cls})" 
            else : 
                exec_code += f".logical_or(t[cls] == {row_cls})"

        d = {'t' : t, 'cls' : cls}
        exec(exec_code, d)
        mask = d['mask']
        t[cls][mask] = 1
        t[cls][torch.logical_not(mask)] = 0

    return t


def shallow_to_rgb(t, n_class, labels):
    'convert a shallow label [1, h, w] to a rgb one [3, h, w]'

    res = t.repeat(3, 1, 1)
    for n in range(n_class):
        mask = (t == (n))
        
        r, g, b = labels[n].color
        res[0][mask] = r
        res[1][mask] = g
        res[2][mask] = b

    return res