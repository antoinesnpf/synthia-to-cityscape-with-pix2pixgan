# -*- coding: utf-8 -*-
from collections import namedtuple
from PIL import Image
import os 
import numpy as np
import matplotlib.pyplot as plt
import random as rd
import torch
import torchvision.transforms as transforms
from torchvision.transforms import InterpolationMode
import time
from data.utils import Label, shallow_to_deep, shallow_to_rgb


DATAFOLDER = 'D:\\dataset\\cityscape'

LABELS = [
    #        name                        row id          id                  
    Label(  'none',                         0 ,          0        ),              
    Label(  'road',                         7 ,          1        ),
    Label(  'sidewalk',                     8 ,          2        ),
    Label(  'building',                     11 ,         3        ),
    Label(  'wall',                         12 ,         4        ),
    Label(  'fence',                        13 ,         5        ),
    Label(  'pole',                         17 ,         6        ),
    Label(  'traffic light',                19 ,         7        ),
    Label(  'vegetation',                   21 ,         8        ),
    Label(  'sky',                          23 ,         9        ),
    Label(  'person',                       24 ,         10       ),
    Label(  'car',                          26 ,         11       ),              
    Label(  'truck',                        27 ,         12       ),              
    Label(  'bicycle',                      33 ,         13       ),   
    Label(  'ego vehicle',                  1 ,          0        ),             
    Label(  'rectification border',         2 ,          0        ),              
    Label(  'out of roi',                   3 ,          0        ),             
    Label(  'static',                       4 ,          0        ),               
    Label(  'dynamic',                      5 ,          0        ),
    Label(  'ground',                       6 ,          0        ),       
    Label(  'rail track',                   10 ,         0        ),  
    Label(  'bridge',                       15 ,         0        ), 
    Label(  'tunnel',                       16 ,         0        ),
    Label(  'terrain',                      22 ,         0        ),
    Label(  'trailer',                      30 ,         0        ),            
    Label(  'train',                        31 ,         0        ),            
    Label(  'motorcycle',                   32 ,         0        ),             
    Label(  'license plate',                -1 ,         0        ),    
    Label(  'parking',                      9 ,          1        ),    
    Label(  'guard rail',                   14 ,         5        ), 
    Label(  'polegroup',                    18 ,         6        ),
    Label(  'traffic sign',                 20 ,         6        ),
    Label(  'rider',                        25 ,         10       ),  
    Label(  'bus',                          28 ,         12       ),             
    Label(  'caravan',                      29 ,         12       )
]

HASH_MAP = {
    0     :    [0, 1, 2, 3, 4, 5, 6, 10, 15, 16, 30, 31, 32, 22, -1] ,          
    1     :    [7, 9] ,          
    2     :    [8] ,          
    3     :    [11],          
    4     :    [12],          
    5     :    [13, 14],          
    6     :    [17, 17, 19],          
    7     :    [19],          
    8     :    [21],          
    9     :    [23],          
    10    :    [24, 25],         
    11    :    [26],         
    12    :    [27, 28, 29],         
    13    :    [33]
}    

class CityscapeDataset:
    
    def __init__(self, 
                 split,
                 image_nb = None,
                 img_size = 256,
                 transform = None, 
                 target_transform = None):

        self.data = [] #RGB images
        self.targets = [] # one channel labels
        self.pads = []
        self.n_class = 14

        if transform is None : 
            transform = transforms.Compose([
                                    transforms.ToTensor(),
                                    transforms.Resize(img_size),
                                    transforms.CenterCrop(img_size),
                                    transforms.Normalize(mean = [0, 0, 0], std = [1, 1, 1])
                                ])
        if target_transform is None : 
            target_transform = transforms.Compose([
                                    transforms.Resize(img_size, InterpolationMode.NEAREST),
                                    transforms.CenterCrop(img_size)
                                ])

        self.transform = transform    
        self.target_transform = target_transform

        if image_nb is None:
            if split == 'train':
                image_nb = 2975
            elif split == 'val' : 
                image_nb = 500

        self._load_data(split, image_nb)

    def _load_data(self, split, image_nb) : 

        'load dataset with random walk sans remise'

        assert split in ['train', 'val']

        segmap_folder = 'gtFine'
        img_folder = 'leftImg8bit'
        label_extension  = '_gtFine_labelIds.png'

        data_temp = []
        targets_temp = []
        for city in os.listdir(os.path.join(DATAFOLDER, img_folder, split)) : 
            for name in os.listdir(os.path.join(DATAFOLDER, img_folder, split, city)) : 
                img_path  = os.path.join(DATAFOLDER, img_folder, split, city, name)
                segmap_path = os.path.join(DATAFOLDER, segmap_folder, split, city, name[:-16] + label_extension)
                data_temp.append(img_path)
                targets_temp.append(segmap_path)

        print(f'maximal dataset len: {len(data_temp)}')
        idxs = rd.sample(list(range(len(data_temp))), image_nb)
        for idx in idxs:
            self.data.append(data_temp[idx])
            self.targets.append(targets_temp[idx])
        self.pads = [rd.randint(0, 600) + rd.randint(0, 600) for _ in range(image_nb)] # choose padding based on triagular distribution

    def _process_data(self, img, target):

        img =  self.transform(img)
        target = self.target_transform(torch.Tensor(target).unsqueeze(0))
        return img, shallow_to_deep(target, self.n_class, HASH_MAP)

    def __getitem__(self, idx):
        """returns: 
            img : tensor of size [3, img_size, img_size]
            target : tesor of size [n_class, img_size, img_size]
        """

        pad = self.pads[idx]
        img = Image.open(self.data[idx]).crop((pad, 0, pad + 800, 800))
        target = np.array(Image.open(self.targets[idx]).crop((pad, 0, pad + 800, 800)))
        
        return self._process_data(img, target)

    def __len__(self):
        return len(self.data)

    def plot(self, k) : 
        img, target = self[k]
        img, target = img.numpy(), torch.argmax(target, axis = 0).unsqueeze(0).numpy()
        plt.imshow(np.transpose(img, (1,2,0)))
        plt.show()
        plt.imshow(np.transpose(target, (1,2,0)))
        plt.show()

    def plot_rgb(self, k):
        img, target = self.to_rgb(k)
        plt.imshow(np.transpose(img, (1,2,0)))
        plt.show()
        plt.imshow(np.transpose(target, (1,2,0)))
        plt.show()

    def to_rgb(self, k) : 
        img, target = self[k]
        target = shallow_to_rgb(torch.argmax(target, axis = 0), self.n_class, LABELS)
        return img, target


if __name__ == '__main__':  
    get_class_distribution = False
    
    split = 'train'
    n_img = None
    t = time.time()
    dataset = CityscapeDataset(split, n_img)
    print(f'Loading time: {time.time() - t}')


    if get_class_distribution:
        pixel_per_class_count = {k : 0 for k in range(dataset.n_class)}
        total = 0

        for k in range(len(dataset)): 
            segmap = dataset[k][1]
            for cls in range(dataset.n_class):
                pixel_per_class_count[cls] += len(np.where(torch.argmax(segmap, axis=0) == cls)[0])
            total += torch.argmax(segmap, axis=0).numpy().size


        for cls in range(dataset.n_class):
            pixel_per_class_count[cls] *= 100/total