# -*- coding: utf-8 -*-
from PIL import Image
import os 
import numpy as np
import matplotlib.pyplot as plt
import random as rd
import torch
from data.utils import Label, shallow_to_deep, shallow_to_rgb
import torchvision.transforms as transforms
from torchvision.transforms import InterpolationMode
import time


# http://synthia-dataset.net/table-classes/


DATAFOLDER = 'D:\\dataset\\synthia'

LABELS =  [
    #         name                                row id         id                 
    Label(   'none',                                0,           0,                   ),
    Label(   'route',                               1,           1,                   ),
    Label(   'trottoir',                            2,           2,                   ),
    Label(   'batiment',                            3,           3,                   ),
    Label(   'muret',                               4,           4,                   ),
    Label(   'palissade',                           5,           5,                   ),
    Label(   'lampadaire',                          6,           6,                   ),
    Label(   'feu de traffic',                      7,           7,                   ),
    Label(   'vegetation',                          9,           8,                   ),
    Label(   'ciel',                                11,          9,                   ),
    Label(   'pieton',                              12,          10,                  ),
    Label(   'voiture',                             14,          11,                  ),
    Label(   'camion',                              15,          12,                  ),
    Label(   'velo',                                19,          13,                  ),
    Label(   'none',                                18,          0,                   ),
    Label(   'poubelles, pub, abris bus',           21,          0,                   ),
    Label(   'barrières de travaux',                22,          0,                   ),
    Label(   'rails',                               23,          0,                   ),
    Label(   'fauteuil roulant',                    25,          0,                   ),
    Label(   'marquage au sol',                     20,          1,                   ),
    Label(   'panneau',                             8,           6,                   ),
    Label(   'montagne',                            10,          3,                   ),
    Label(   'bus',                                 16,          12,                  )
]

HASH_MAP = {
          0        :    [0, 18, 22, 23, 25],
          1        :    [1, 20] ,
          2        :    [2] ,
          3        :    [3, 10] ,
          4        :    [4] ,
          5        :    [5] ,
          6        :    [6, 8] ,
          7        :    [7] ,
          8        :    [9] ,
          9        :    [11] ,
          10       :    [12] ,
          11       :    [14] ,
          12       :    [15, 16] ,
          13       :    [19] 
}

PIXEL_PER_CLASS = {
    0: 1.0468292236328125,
    1: 35.00637359619141,
    2: 3.8935668945312503,
    3: 27.513241577148438,
    4: 2.0538955688476563,
    5: 0.098797607421875,
    6: 1.3623977661132813,
    7: 0.12096099853515625,
    8: 3.5834808349609375,
    9: 19.822593688964844,
    10: 0.46618194580078126,
    11: 4.951416015625,
    12: 0.048501586914062506,
    13: 0.0317626953125
}


class SynthiaDataset(torch.utils.data.Dataset) : 
    
    def __init__(self, 
                 split,
                 image_nb,
                 img_size = 256,
                 transform = None, 
                 target_transform = None):

        self.data = [] #RGB images
        self.targets = [] # one channel labels
        self.n_class = 14

        if transform is None : 
            transform = transforms.Compose([
                                    transforms.ToTensor(),
                                    transforms.Resize(img_size),
                                    transforms.CenterCrop(img_size),
                                    transforms.Normalize(mean = [0.485, 0.456, 0.406, 0], std = [0.229, 0.224, 0.225, 1])
                                ])

            self.inverse_transform = transforms.Compose([
                                    transforms.Normalize(mean = [0., 0., 0.], std = [1 / 0.229, 1 / 0.224, 1 / 0.225]), 
                                    transforms.Normalize(mean = [-0.485, -0.456, -0.406], std = [1., 1., 1.])
                                ])

        if target_transform is None : 
            target_transform = transforms.Compose([
                                    transforms.Resize(img_size, InterpolationMode.NEAREST),
                                    transforms.CenterCrop(img_size)
                                ])

        self.transform = transform    
        self.target_transform = target_transform

        self._load_data(split, image_nb)

    def _load_data(self, split, image_nb) : 

        'load dataset with random walk'

        assert split in ['train', 'test']

        data_temp = []
        targets_temp = []

        for n1 in [file for file in os.listdir(os.path.join(DATAFOLDER, split)) if 'test' in file and ('weather_2' in file or 'weather_3' in file)]:
            for n2 in os.listdir(os.path.join(DATAFOLDER, split, n1)):
                for name in os.listdir(os.path.join(DATAFOLDER, split, n1, n2, 'SemSeg')):

                    img_path  = os.path.join(DATAFOLDER, split, n1, n2, 'RGB', name)
                    segmap_path = os.path.join(DATAFOLDER, split, n1, n2, 'SemSeg', name)

                    data_temp.append(img_path)
                    targets_temp.append(segmap_path)

        print(f'maximal dataset len: {len(data_temp)}')
        idxs = rd.sample(list(range(len(data_temp))), image_nb)
        for idx in idxs:
            self.data.append(data_temp[idx])
            self.targets.append(targets_temp[idx])

    def _process_data(self, img, target):

        img =  self.transform(img)[0:3]
        target = self.target_transform(torch.Tensor(target).unsqueeze(0))
        return img, shallow_to_deep(target, self.n_class, HASH_MAP)

    def __getitem__(self, idx):
        """returns: 
            img : tensor of size [3, img_size, img_size]
            target : tesor of size [n_class, img_size, img_size]
        """
        img = Image.open(self.data[idx])
        target = np.array(Image.open(self.targets[idx]))[:,:,0]

        return self._process_data(img, target)

    def __len__(self):
        return len(self.data)

    def plot(self, k) : 
        img, target = self[k]
        img = self.inverse_transform(img)
        img, target = img.numpy(), torch.argmax(target, axis = 0).unsqueeze(0).numpy()
        plt.imshow(np.transpose(img, (1,2,0)))
        plt.show()
        plt.imshow(np.transpose(target, (1,2,0)))
        plt.show()

    def plot_rgb(self, k):
        img, target = self.to_rgb(k)
        img = self.inverse_transform(img)
        plt.imshow(np.transpose(img, (1,2,0)))
        plt.show()
        plt.imshow(np.transpose(target, (1,2,0)))
        plt.show()

    def to_rgb(self, k) : 
        img, target = self[k]
        target = shallow_to_rgb(torch.argmax(target, axis = 0), self.n_class, LABELS)
        return img, target


def plot_feature(n):
    t = time.time()
    loc_datafolder = os.path.join(DATAFOLDER, 'train') 

    init_bool = True
    while init_bool  : 
        n1 = rd.choice([file for file in os.listdir(loc_datafolder) if 'test' in file])
        n2 = rd.choice(os.listdir(os.path.join(loc_datafolder, n1)))
        n3 = 'SemSeg'
        name = rd.choice(os.listdir(os.path.join(loc_datafolder, n1, n2, n3)))
        img = Image.open(os.path.join(loc_datafolder, n1, n2, n3, name))
        segmap = np.array(img)[:,:,0]
        init_bool = (len(np.where(segmap == n)[0]) == 0)

        if time.time()- t > 10 : 
            print(f'no img for n = {n}')
            return None

    segmap[segmap == n] = 50
    plt.imshow(segmap)
    plt.show()
    return n1, n2, name

def get_dataset_cardinal():
    c = 0
    for n0 in ['test', 'train'] : 
        for n1 in [file for file in os.listdir(os.path.join(DATAFOLDER, n0)) if 'test' in file] : 
            for n2 in os.listdir(os.path.join(DATAFOLDER, n0, n1)):
                n3 = 'SemSeg'
                c += len(os.listdir(os.path.join(DATAFOLDER, n0, n1, n2, n3))) 
    return c

if __name__ == '__main__':
    get_class_distribution = False
    #plot_feature(14)


    n_point = 500

    t = time.time()
    dataset = SynthiaDataset(split = 'test', image_nb = n_point)
    print(f'Loading time: {time.time() - t}')

    if get_class_distribution:
        pixel_per_class_count = {k : 0 for k in range(dataset.n_class)}
        total = 0

        for k in range(n_point): 
            segmap = dataset[k][1]
            for cls in range(dataset.n_class):
                pixel_per_class_count[cls] += len(np.where(torch.argmax(segmap, axis=0) == cls)[0])
            total += torch.argmax(segmap, axis=0).numpy().size


        for cls in range(dataset.n_class):
            pixel_per_class_count[cls] *= 100/total

