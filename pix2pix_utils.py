import os
import shutil
import PIL
import torchvision.transforms as transforms
from utils import run
from cfg import GAN_PATH, REPO_PATH

def save_data(dataset, partition, target_dir):

    toPIL = transforms.ToPILImage()
    for idx in range(len(dataset)) : 
        img, segmap = dataset.to_rgb(idx)
        img, segmap = toPIL(img.float()), toPIL(segmap.float()/255)
        img.save(os.path.join(target_dir, 'pics', partition, f'{idx}.png'))
        segmap.save(os.path.join(target_dir, 'labels', partition, f'{idx}.png'))

    print(f'{len(dataset)} labels and images saved in partition {partition}')

        
def create_dataset_folder(dataset_path):
    if not os.path.exists(dataset_path) : 
        os.makedirs(os.path.join(dataset_path, 'pics', 'train'))
        os.makedirs(os.path.join(dataset_path, 'pics', 'test'))
        os.makedirs(os.path.join(dataset_path, 'labels', 'train'))
        os.makedirs(os.path.join(dataset_path, 'labels', 'test'))
        os.makedirs(os.path.join(dataset_path, 'aligned'))


def reset_dataset_folder(dataset_path):
    if  os.path.exists(dataset_path) :
        shutil.rmtree(dataset_path)
        create_dataset_folder(dataset_path)


def align_dataset(dataset_path) :
    """creates the `aligned` folder in dataset_path and saves the aligned dataset within it"""
    cmd    =     [
        os.path.join(REPO_PATH, 'venv', 'Scripts', 'python'),
        os.path.join(GAN_PATH, 'datasets', 'combine_A_and_B.py'), 
        '--fold_A', os.path.join(dataset_path, 'labels'), 
        '--fold_B', os.path.join(dataset_path, 'pics'),
        '--fold_AB', os.path.join(dataset_path, 'aligned'),
        '--no_multiprocessing'
    ]
    run(cmd)




def train_pix2pix(dataset_path, 
                    model_name,
                    kwargs): 
            
    """[summary] Train the pix2pix networks using the dataset of dataset_path with the train partition. 
    The trained model is saved in pix2pix/checkpoints/[model_name]

    Args:
        dataset_path (str, optional): [description]. Path to the folder where the dataset to be used 
        is stored.
    """

    cmd    =     [
        os.path.join(REPO_PATH, 'venv', 'Scripts', 'python'),
        os.path.join(GAN_PATH, 'train.py'), 
        '--dataroot', os.path.join(dataset_path, 'aligned'), 
        '--name', model_name, 
        '--model', 'pix2pix',
        '--checkpoints_dir', 'pix2pix_checkpoints', 
        '--display_id', '0'
    ]

    for param in kwargs.keys(): 
        value = kwargs[param]
        if isinstance(value, bool):
            if value : 
                cmd.append(f'--{param}')
        else : 
            cmd.append(f'--{param}')
            cmd.append(f'{value}')

    run(cmd)


def test_pix2pix(dataset_path, 
                model_name,
                kwargs): 

    """ Test a pretrained model using the dataset_path with the test partition. Results can be 
    observed in pix2pix_results/[model_name] 

    Args:
        dataset_path (str): Path to the folder where the dataset to be used 
        is stored.
        model_name (str): Name of the model to be loaded and tested.
    """

    cmd    =     [
        os.path.join(REPO_PATH, 'venv', 'Scripts', 'python'), 
        os.path.join(GAN_PATH, 'test.py'), 
        '--dataroot', os.path.join(dataset_path, 'aligned'), 
        '--name', model_name, 
        '--model', 'pix2pix',
        '--checkpoints_dir', 'pix2pix_checkpoints',
        '--results_dir', 'pix2pix_results', 
        ]

    for param in kwargs.keys(): 
        value = kwargs[param]
        if isinstance(value, bool):
            if value : 
                cmd.append(f'--{param}')
        else : 
            cmd.append(f'--{param}')
            cmd.append(f'{value}')

    run(cmd)



