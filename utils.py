# -*- coding: utf-8 -*-

import subprocess
import numpy as np 
import torchvision.transforms as transforms
import matplotlib.pyplot as plt

class AverageMeter(object):
    """ Computes and stores the average and current value"""
    def __init__(self, name = ''):
        self.reset()
        self.name = name
        self.values = []
    
    def reset(self):
        self.val = 0.
        self.avg = 0.
        self.sum = 0.
        self.count = 0

    def new_epoch(self):
        self.values.append(self.avg)
        self.reset()
    
    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def plot(self) : 
        plt.figure()
        plt.plot(np.arange(len(self.values)), self.values)
        plt.ylabel(self.name)
        plt.xlabel('epoch number')
        plt.show()

    def cross_plot(self, *args) : 
        """[summary] Plot on the same the graph of the variable held in self as well as 
        all the other variables held in AverageMeter objects [*args]

        Input:
            *args: list of AverageMetter objects whose variables should be ploted
        """
        plt.figure()
        legend = []
        plt.plot(np.arange(len(self.values)), self.values)
        legend.append(self.name)
        for obj in args : 
            plt.plot(np.arange(len(obj.values)), obj.values)
            legend.append(obj.name)
        plt.xlabel('epoch number')
        plt.legend(legend)
        plt.show()

def pixel_acc(pred, target):
    correct = (pred == target).sum()
    total   = (target == target).sum()
    return correct / total

def get_iou(pred, target, n_class):
    # Calculates class intersections over unions
    ious = []
    for cls in range(n_class):
        pred_inds = (pred == cls)
        target_inds = (target == cls)
        intersection = pred_inds[target_inds].sum()
        union = pred_inds.sum() + target_inds.sum() - intersection
        if union == 0:
            ious.append(float('nan'))  # if there is no ground truth, do not include in evaluation
        else:
            ious.append(float(intersection) / max(union, 1))
        # print("cls", cls, pred_inds.sum(), target_inds.sum(), intersection, float(intersection) / max(union, 1))
    return ious



def run(cmd) : 
    p = subprocess.Popen(cmd,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT)

    for line in iter(p.stdout.readline, b''):
        line_str = line.rstrip().decode()
        if 'InterpolationMode' not in line_str and 'warnings.warn' not in line_str: 
            print(">>> " + line.rstrip().decode())