# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import models
from torchvision.models.vgg import VGG

#code adapted from https://github.com/pochih/FCN-pytorch

class FCNs(nn.Module):

    def __init__(self, pretrained_net, n_class):
        super().__init__()
        self.n_class = n_class
        self.pretrained_net = pretrained_net
        self.relu    = nn.ReLU(inplace=True)
        self.convtrans1 = nn.ConvTranspose2d(512, 512, kernel_size=3, stride=2, padding=1, dilation=1, output_padding=1)
        self.batch1     = nn.BatchNorm2d(512)
        self.convtrans2 = nn.ConvTranspose2d(512, 256, kernel_size=3, stride=2, padding=1, dilation=1, output_padding=1)
        self.batch2     = nn.BatchNorm2d(256)
        self.convtrans3 = nn.ConvTranspose2d(256, 128, kernel_size=3, stride=2, padding=1, dilation=1, output_padding=1)
        self.batch3     = nn.BatchNorm2d(128)
        self.convtrans4 = nn.ConvTranspose2d(128, 64, kernel_size=3, stride=2, padding=1, dilation=1, output_padding=1)
        self.batch4     = nn.BatchNorm2d(64)
        self.convtrans5 = nn.ConvTranspose2d(64, 32, kernel_size=3, stride=2, padding=1, dilation=1, output_padding=1)
        self.batch5     = nn.BatchNorm2d(32)
        self.classifier = nn.Conv2d(32, n_class, kernel_size=1)

    def forward(self, x):
        output = self.pretrained_net(x)
        x5 = output['x5']  
        x4 = output['x4']  
        x3 = output['x3']  
        x2 = output['x2']  
        x1 = output['x1']  

        x = self.batch1(self.relu(self.convtrans1(x5)))      
        x = x + x4                                
        x = self.batch2(self.relu(self.convtrans2(x)))   
        x = x + x3                                
        x = self.batch3(self.relu(self.convtrans3(x)))  
        x = x + x2                                
        x = self.batch4(self.relu(self.convtrans4(x)))  
        x = x + x1                                
        x = self.batch5(self.relu(self.convtrans5(x)))  
        x = self.classifier(x)                 

        return x 


class VGGNet(VGG):
    def __init__(self, pretrained=True, requires_grad=True, remove_fc=True, show_params=False):
        super().__init__(make_layers())
        self.struct =  ((0, 5), (5, 10), (10, 17), (17, 24), (24, 31))
        if pretrained:
            self.load_state_dict(models.vgg16(pretrained=True).state_dict())

        if not requires_grad:
            for param in super().parameters():
                param.requires_grad = False

        if remove_fc:  
            del self.classifier

        if show_params:
            for name, param in self.named_parameters():
                print(name, param.size())

    def forward(self, x):
        output = {}
        for idx in range(len(self.struct)):
            for layer in range(self.struct[idx][0], self.struct[idx][1]):
                x = self.features[layer](x)
            output["x%d"%(idx+1)] = x

        return output


def make_layers(batch_norm=False):
    cfg = [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M']
    layers = []
    in_channels = 3
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return nn.Sequential(*layers)


