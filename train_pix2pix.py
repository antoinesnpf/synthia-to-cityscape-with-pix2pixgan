import os
import copy
from data.cityscape import CityscapeDataset
from pix2pix_utils import create_dataset_folder, reset_dataset_folder, align_dataset, train_pix2pix, test_pix2pix, save_data


load_data = False
save_data_for_training = False
align = True 
train_ = False 
test_ = True 
gan_augment_ = False

#model parameters
model_name = 'pix2pix_cityscape_v0'
dataset_name = 'cityscape_v0'
dataset_folder = 'D:\\dataset\\pix2pix'
dataset_path = os.path.join(dataset_folder, dataset_name)

test_kwargs = {
    'input_nc'       : 3,
    'output_nc'      : 3,
    'ngf'            : 64,
    'ndf'            : 64,
    'netG'           : 'unet_256',
    'netD'           : 'basic',
    'n_layers_D'     : 3,
    'norm'           : 'batch',
    'init_type'      : 'normal',
    'init_gain'      : 0.02,
    'no_dropout'     : False, 
}

train_kwargs = copy.copy(test_kwargs)
train_kwargs['n_epochs'] = 100
train_kwargs['n_epochs_decay'] = 100
train_kwargs['load_size'] = 256
train_kwargs['num_threads'] = 1


#dataset parameters
n_img_train = 2975
n_img_test = 25

# set up dataset
if load_data : 
    trainset  = CityscapeDataset('train', n_img_train)
    testset  = CityscapeDataset('val', n_img_test)

# save data for training
if save_data_for_training:
    reset_dataset_folder(dataset_path)
    create_dataset_folder(dataset_path)
    save_data(trainset, partition = 'train', target_dir=dataset_path)
    save_data(testset, partition = 'test', target_dir=dataset_path)

#perform alignment operation on the dataset
if align :
    align_dataset(dataset_path)
    
# create and train a pix2pix GAN
if train_ :
    train_pix2pix(dataset_path, model_name, train_kwargs)

# test a pretrained pix2xpix GAN
if test_ : 
    test_pix2pix(dataset_path, model_name, test_kwargs)


