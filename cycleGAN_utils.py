import os
import shutil
import PIL
import torchvision.transforms as transforms
from utils import run
from cfg import GAN_PATH, REPO_PATH


def save_data(dataset1, dataset2, partition, target_dir):

    toPIL = transforms.ToPILImage()
    n_max = min(len(dataset1), len(dataset2))
    for idx in range(n_max) : 
        img1, segmap = dataset1.to_rgb(idx)
        img2, segmap = dataset2.to_rgb(idx)
        img1, img2 = toPIL(img1.float()), toPIL(img2.float())
        img1.save(os.path.join(target_dir, partition + 'A', f'{idx}.png'))
        img2.save(os.path.join(target_dir, partition + 'B', f'{idx}.png'))

    print(f'{n_max} images saved in partition {partition}')


def create_dataset_folder(dataset_path):
    if not os.path.exists(dataset_path) : 
        os.makedirs(os.path.join(dataset_path, 'trainA'))
        os.makedirs(os.path.join(dataset_path, 'testA'))
        os.makedirs(os.path.join(dataset_path, 'trainB'))
        os.makedirs(os.path.join(dataset_path, 'testB'))

def reset_dataset_folder(dataset_path):
    if  os.path.exists(dataset_path) :
        shutil.rmtree(dataset_path)
        create_dataset_folder(dataset_path)

def align_dataset(dataset_path) :
    """creates the `aligned` folder in dataset_path and saves the aligned dataset within it"""
    cmd    =     [
        os.path.join(REPO_PATH, 'venv', 'Scripts', 'python'),
        os.path.join(GAN_PATH, 'datasets', 'combine_A_and_B.py'), 
        '--fold_A', os.path.join(dataset_path, 'pics1'), 
        '--fold_B', os.path.join(dataset_path, 'pics2'),
        '--fold_AB', os.path.join(dataset_path, 'unaligned'),
        '--no_multiprocessing'
    ]
    run(cmd)


def train_cycleGAN(dataset_path, 
                    model_name,
                    kwargs): 
            
    """[summary] Train the cycleGAN networks using the dataset of dataset_path with the train partition. 
    The trained model is saved in cycleGAN_checkpoints/[model_name]

    Args:
        dataset_path (str, optional): [description]. Path to the folder where the dataset to be used 
        is stored.
    """

    cmd    =     [
        os.path.join(REPO_PATH, 'venv', 'Scripts', 'python'),
        os.path.join(GAN_PATH, 'train.py'), 
        '--dataroot', os.path.join(dataset_path), 
        '--name', model_name, 
        '--model', 'cycle_gan',
        '--checkpoints_dir', 'cycleGAN_checkpoints', 
        '--display_id', '0'
    ]

    for param in kwargs.keys(): 
        value = kwargs[param]
        if isinstance(value, bool):
            if value : 
                cmd.append(f'--{param}')
        else : 
            cmd.append(f'--{param}')
            cmd.append(f'{value}')

    run(cmd)


def test_cycleGAN(dataset_path, 
                model_name,
                kwargs): 

    """ Test a pretrained model using the dataset_path with the test partition. Results can be 
    observed in cycleGAN_results/[model_name] 

    Args:
        dataset_path (str): Path to the folder where the dataset to be used 
        is stored.
        model_name (str): Name of the model to be loaded and tested.
    """

    cmd    =     [
        os.path.join(REPO_PATH, 'venv', 'Scripts', 'python'), 
        os.path.join(GAN_PATH, 'test.py'), 
        '--dataroot', os.path.join(dataset_path), 
        '--name', model_name, 
        '--model', 'cycle_gan',
        '--checkpoints_dir', 'cycleGAN_checkpoints',
        '--results_dir', 'cycleGAN_results', 
        ]

    for param in kwargs.keys(): 
        value = kwargs[param]
        if isinstance(value, bool):
            if value : 
                cmd.append(f'--{param}')
        else : 
            cmd.append(f'--{param}')
            cmd.append(f'{value}')

    run(cmd)