
import os
from cfg import REPO_PATH
os.chdir(REPO_PATH)
import numpy as np
import time
import random 
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import pickle
import matplotlib.pyplot as plt
from torch.optim import lr_scheduler
from torch.utils.data import DataLoader
 #from torch.utils.tensorboard import SummaryWriter

from fcn import VGGNet, FCNs, FCN32s, FCN16s, FCN8s
from utils import AverageMeter, get_iou, pixel_acc
from data.synthia import SynthiaDataset, PIXEL_PER_CLASS, LABELS
from data.utils import shallow_to_rgb
import torchvision.transforms as transforms



###########################
# training FCN on Synthia #
###########################



# prepare cuda
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
n_class       = 14

# main hyperparameters
batch_size     = 12
epochs         = 60
lr             = 1e-3   
momentum       = 0.9
w_decay        = 0
step_size      = 25
gamma          = 0.8
weighting      = False

img_size        = 256 
n_train_img     = 500
n_test_img      = 100
train_partition = 'train'
test_partition  = 'train'

configs    = f"FCNs--BCEWithLogits-batch={batch_size}-epoch={epochs}--dataset-n_class={n_class}-weighting={weighting}-n_train={n_train_img}-n_test={n_test_img}\
-test_parition={test_partition}"


# prepare dir for models and scores 
checkpoints_dir = "c:\\Users\\Antoine\\Desktop\\GIT\\synthia-to-cityscape-with-pix2pixgan\\fcn_checkpoints"
model_path = os.path.join(checkpoints_dir, configs)
if not os.path.exists(model_path) : 
    os.makedirs(model_path)

# prepare data
t = time.time()
trainset = SynthiaDataset(split = test_partition, image_nb = n_train_img, img_size = img_size)
testset = SynthiaDataset(split = test_partition, image_nb = n_test_img, img_size = img_size)
trainloader = DataLoader(trainset, batch_size=batch_size, shuffle=True)
testloader = DataLoader(testset, batch_size=1)
print(f'Loading time: {time.time() - t}')


# prepare models
backbone = VGGNet(requires_grad=True, remove_fc=True)
model = FCNs(pretrained_net=backbone, n_class=n_class)
model.to(device)

# loss function, optimizer, scheduler 
weight_list = np.array([1 / PIXEL_PER_CLASS[cls] for cls in range(n_class)])
weight_list = weight_list / weight_list.sum()
weights = torch.ones((n_class, img_size, img_size)).to(device)
for k, w in enumerate(weight_list) : 
    weights[k, :, :] = w

criterion = nn.BCEWithLogitsLoss(reduction = 'none')
optimizer = optim.RMSprop(model.parameters(), lr=lr, momentum=momentum, weight_decay=w_decay)
scheduler = lr_scheduler.StepLR(optimizer, step_size=step_size, gamma=gamma)  # decay LR by a factor gamma every step_size epochs

# writer and trainer
#writer = SummaryWriter(comment = configs)
TrainingLoss = AverageMeter(name = 'Training Loss')
TestingLoss = AverageMeter(name = 'Testing Loss')
PixelAcc = AverageMeter(name = 'Pixel Accuracy')
IoUs = [AverageMeter(name = f'IoU for class {k}') for k in range(n_class)]

def train(save_model = True):
    # training
    model.train()
    for epoch in range(epochs):
        ts = time.time()

        for iter_nb, batch in enumerate(trainloader):
            optimizer.zero_grad()
            current_batch_size = len(batch)
            inputs, labels = batch

            inputs = inputs.to(device)
            labels = labels.to(device)

            # forward and backward pass
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            if weighting : 
                loss = (loss * weights).mean()
            loss = loss.mean()
            loss.backward()
            TrainingLoss.update(loss.item())
            #writer.add_scalar('TRAIN/Training Loss', loss.item(), epoch)

            # update model.parameters()
            optimizer.step()
            scheduler.step()
            
            if iter_nb % 20 == 0 : 
                print(f'epoch [{epoch}/{epochs}]iter [{iter_nb}/{len(trainloader)}]')

        print("Finish epoch {}, time elapsed {}".format(epoch, time.time() - ts))
        # save the model after each epoch for backup
        if save_model : 
            torch.save({'epoch': epoch, 'state_dict': model.state_dict()}, os.path.join(model_path, f'epoch_{epoch}.pth.tar'))
        
        TrainingLoss.new_epoch()

        # validate the model
        test(epoch)      

def test(epoch) :
    # activating evaluation mode
    model.eval()

    for iter_nb, batch in enumerate(testloader):
        inputs, labels = batch
        inputs = inputs.to(device)
        labels = labels.to(device)

        # forward
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        #loss = (loss * weights).mean()
        loss = loss.mean()
        loss.backward()
        TestingLoss.update(loss.item())
        #writer.add_scalar('TEST/Testing Loss', loss.item(), epoch)

        # generate segmentation maps
        pred = outputs.argmax(axis=1).unsqueeze(0)
        target = labels.argmax(axis=1).unsqueeze(0)

        pred = pred.data.cpu().numpy()
        target = target.data.cpu().numpy()

        # calculate IoUs
        for i, iou in enumerate(get_iou(pred, target, n_class)): 
            IoUs[i].update(iou)
            #writer.add_scalar(f'TEST/IoU for class {i}', iou, epoch)

        acc = pixel_acc(pred, target)
        PixelAcc.update(acc)
        #writer.add_scalar('TEST/Pixel accuracy', pixel_acc, epoch)

    TestingLoss.new_epoch()
    PixelAcc.new_epoch()
    for IoU in IoUs:
        IoU.new_epoch()


def save_logs(logs):
    with open(os.path.join(model_path, 'score.txt'), 'w') as file:
        for key in logs.keys():
            line = f'{key}: {logs[key]}'
            file.write(key)
            

print("Configs:", configs)

train()
#writer.flush()
#writer.close()
TrainingLoss.plot()
TestingLoss.plot()

logs = {
    'TrainingLoss' : TrainingLoss, 
    'TestingLoss' : TestingLoss, 
    'IoUs' : IoUs, 
    'PixelAcc' : PixelAcc
}

save_logs(logs)
inverse_transform = transforms.Compose([
                        transforms.Normalize(mean = [0., 0., 0.], std = [1 / 0.229, 1 / 0.224, 1 / 0.225]), 
                        transforms.Normalize(mean = [-0.485, -0.456, -0.406], std = [1., 1., 1.])
            ])



img_nb = 3
batch, segmap = trainset[img_nb]
batch = batch.to(device)

pred = shallow_to_rgb(torch.argmax(model(batch.unsqueeze(0)).squeeze(), axis = 0), n_class, LABELS)
img, target = trainset.to_rgb(img_nb)
img = inverse_transform(img)
plt.imshow(np.transpose(img, (1,2,0)))
plt.show()
plt.imshow(np.transpose(target, (1,2,0)))
plt.show()
plt.imshow(np.transpose(pred.cpu().numpy(), (1,2,0)))


