# SYNTHIA to Cityscape with pix2pixGAN

Image to image translation from SYNTHIA to Cityscape. Can be used to perfrom data augmentation on a version of CItyscape reduced to 13 classes.

download the cityscape dataset : https://www.cityscapes-dataset.com/
download the synthia dataset : https://synthia-dataset.net/

Two methods are compared:
1) With Pix2pix trained on Cityscape and its annotations(aligned dataset) and a semantic correspondance between the annotation of the two datasets
2) With Cycle GAN trained on SYNTHIA and Cityscape (unaligned dataset)
